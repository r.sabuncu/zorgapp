package adsd.hu.zorgapp.Controller;

import adsd.hu.zorgapp.Main;
import adsd.hu.zorgapp.Model.PatientenLijst;
import adsd.hu.zorgapp.Output.ReadWriteIO;
import adsd.hu.zorgapp.View.PatientViewCommandPrompt;

import java.io.File;

public class PatientController {

    PatientenLijst patientenList = new PatientenLijst();
    PatientViewCommandPrompt view;

    public PatientController() {
        File profile = new File(Main.basedir + "/patienten.json");
        boolean exists = profile.exists();

        if (exists){
            ReadWriteIO readWriteIO = new ReadWriteIO();
            patientenList.list = readWriteIO.Read();
        } else {
            System.out.println("Profile dont exists");
        }

        view = new PatientViewCommandPrompt(patientenList);
    }
}

