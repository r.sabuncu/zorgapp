package adsd.hu.zorgapp.Model;

import java.io.*;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public class PatientModel implements Serializable {

    public GewichtsRegistratieLijst gewichtsRegistratieLijst = new GewichtsRegistratieLijst();
    public MedicatieLijst medicatieLijst = new MedicatieLijst();

    private int id;
    private String firstname;
    private String surname;
    private String callName;
    private String address;
    private LocalDate dateOfBirth;
    private double weight;
    private double length;
    private int age;

    public PatientModel(int id,String firstname, String surname, String callName, String address, LocalDate dateOfBirth, double weight, double length){
        this.id = id;
        this.firstname = firstname;
        this.surname = surname;
        this.callName = callName;
        this.address = address;
        this.dateOfBirth = dateOfBirth;
        this.weight = weight;
        this.length = length;
    }

    public PatientModel(int id, String firstname, String surname, LocalDate dateOfBirth){
        this.id = id;
        this.firstname = firstname;
        this.surname = surname;
        this.dateOfBirth = dateOfBirth;

        LocalDate now = LocalDate.now();
        this.age = (int)ChronoUnit.YEARS.between(this.getDateOfBirth(), now);
    }

    public PatientModel() {}

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public int getAge() {
       return this.age;
    }
    public void setAge(int age){
        this.age = age;
    }
    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }
    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;

        LocalDate now = LocalDate.now();
        this.age = (int)ChronoUnit.YEARS.between(this.getDateOfBirth(), now);
    }
    public String getFirstname() {
        return firstname;
    }
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }
    public String getSurname() {
        return surname;
    }
    public void setSurname(String surname) {
        this.surname = surname;
    }
    public String getCallName() {
        return callName;
    }
    public void setCallName(String callName) {
        this.callName = callName;
    }
    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }
    public double getWeight() {
        return weight;
    }
    public void setWeight(double weight) {
        this.weight = weight;
    }
    public double getLength() {
        return length;
    }
    public void setLength(double length) {
        this.length = length;
    }

    /* ------------ Methods ------------ */
    public double calcBMI(){
        return this.weight / (this.length * this.length);
    }

    public String roundBMI(double BMI, String pattern){
        DecimalFormat df = new DecimalFormat(pattern);
        return df.format(BMI);
    }




    /* Voor swing */
    public PatientModel readPatientObject() {
        PatientModel model = null;

        try {
            FileInputStream fileInputStream = new FileInputStream("profile.txt");
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);

            model = (PatientModel) objectInputStream.readObject();
            objectInputStream.close();
        } catch (Exception e) {
            System.out.println(2);
        }
        return model;
    }

    public void saveAndExit(boolean exit) {
        try {
            FileOutputStream fileOutputStream = new FileOutputStream("profile.txt");
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);

            objectOutputStream.writeObject(this);
            objectOutputStream.flush();
            objectOutputStream.close();
        } catch (Exception e){
            System.out.println(e);
        }

        if (exit) System.exit(0);
    }



}
