package adsd.hu.zorgapp.Model;

import java.time.LocalDateTime;

public class GewichtsRegistratie {

    private int id;
    private LocalDateTime localDateTime = LocalDateTime.now();
    private final double weight;
    private final double length;
    private final double bmi;

    public int getId() {
        return id;
    }

    public LocalDateTime geLocalDateTime() {
        return localDateTime;
    }

    public double getWeight() {
        return weight;
    }

    public double getLength() {
        return length;
    }

    public double getBmi() {
        return bmi;
    }

    public double calcBMI(){
        return (this.weight / this.length / this.length) * 10000;
    }

    public double roundBMI(double BMI){
        String str = Double.toString(BMI);
        return Double.parseDouble(str.substring(0,5));
    }

    public GewichtsRegistratie(int id, double weight, double length) {
        this.id = id;
        this.weight = weight;
        this.length = length;
        this.bmi = roundBMI(calcBMI());
    }
    public GewichtsRegistratie(int id, LocalDateTime datetime, double weight, double length) {
        this.id = id;
        this.localDateTime = datetime;
        this.weight = weight;
        this.length = length;
        this.bmi = roundBMI(calcBMI());
    }
}
