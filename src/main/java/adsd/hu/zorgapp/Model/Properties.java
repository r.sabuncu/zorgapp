package adsd.hu.zorgapp.Model;

import adsd.hu.zorgapp.Main;
import java.io.*;

public class Properties {
    private int weightRegistrationID;
    private int medicationID;
    private int patientID;

    public int getWeightRegistrationID() {
        return weightRegistrationID;
    }
    public int getMedicationID() {
        return medicationID;
    }
    public int getPatientID() {
        return patientID;
    }

    public int getAndRaiseWeightRegistrationID() {
        int id = this.weightRegistrationID;
        this.weightRegistrationID++;
        writeProperties();
        return id;
    }

    public int getAndRaiseMedicationID() {
        int id = this.medicationID;
        this.medicationID++;
        writeProperties();
        return id;
    }

    public int getAndRaisePatientID() {
        int id = this.patientID;
        this.patientID++;
        writeProperties();
        return id;
    }

    public Properties() {
        try (InputStream input = new FileInputStream(Main.basedir+"/config.properties")) {
            java.util.Properties prop = new java.util.Properties();
            prop.load(input);
            this.weightRegistrationID = Integer.parseInt(prop.getProperty("GewichtsRegistratieID"));
            this.medicationID = Integer.parseInt(prop.getProperty("MedicatieID"));
            this.patientID = Integer.parseInt(prop.getProperty("PatientID"));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public void writeProperties(){
        try (OutputStream output = new FileOutputStream("config.properties")) {
            java.util.Properties prop = new java.util.Properties();
            prop.setProperty("MedicatieID", String.valueOf(this.medicationID));
            prop.setProperty("GewichtsRegistratieID", String.valueOf(this.weightRegistrationID));
            prop.setProperty("PatientID", String.valueOf(this.patientID));
            prop.store(output, null);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
