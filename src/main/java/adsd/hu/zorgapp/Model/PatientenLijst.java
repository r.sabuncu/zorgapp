package adsd.hu.zorgapp.Model;

import adsd.hu.zorgapp.Output.ReadWriteIO;

import java.util.ArrayList;
import java.util.Iterator;

public class PatientenLijst {
    public ArrayList<PatientModel> list = new ArrayList<PatientModel>();

    public void add(PatientModel patient){
        list.add(patient);
    }

    public void remove(int id) {
        PatientModel patientRemove = null;

        for (Iterator it = list.iterator(); it.hasNext(); ) {
            PatientModel patient = (PatientModel) it.next();
            if (patient.getId() == id) {
                patientRemove = patient;
            }

            list.remove(patientRemove);
        }
    }

    public void writeJsonAndExit(boolean exit){
        ReadWriteIO readWriteIO = new ReadWriteIO();
        readWriteIO.Write(list);

        if (exit) System.exit(0);
    }


}
