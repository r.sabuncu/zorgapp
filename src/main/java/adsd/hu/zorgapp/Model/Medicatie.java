package adsd.hu.zorgapp.Model;

public class Medicatie {
    private final int id;
    private final String name;
    private final String omschrijving;
    private final String type;
    private String dosering;

    public Medicatie(int id,String name, String omschrijving, String type, String dosering){
        this.id = id;
        this.name = name;
        this.omschrijving = omschrijving;
        this.type = type;
        this.dosering = dosering;
    }

    public int getId() {
        return id;
    }
    public String getName() {
        return name;
    }
    public String getOmschrijving() {
        return omschrijving;
    }
    public String getType() {
        return type;
    }
    public String getDosering() {
        return dosering;
    }




}
