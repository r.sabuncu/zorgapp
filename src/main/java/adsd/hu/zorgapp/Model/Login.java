package adsd.hu.zorgapp.Model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

import static java.lang.System.out;

public class Login {
    private final String password = "0318";
    private boolean loggedIn = false;
    private static final Logger logger = LogManager.getLogger(Login.class);

    public boolean authorize(Scanner scan){
        if (!loggedIn){
            out.print("\u001B[37m" + "Please fill in your password> \u001B[32m");
            String pass = scan.nextLine();
            if(this.password.equals(pass)){
                loggedIn = true;
                logger.info("Health care provider logged in successful");
                return true;
            } else {
                out.println("\u001B[31m"+"Wrong password!"+"\u001B[30m");
                logger.warn("Health care provider loggin faild");
                loggedIn = false;
                return false;
            }
        } else {
            return true;
        }
    }




}
