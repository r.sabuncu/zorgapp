package adsd.hu.zorgapp.Model;

import java.util.ArrayList;

public class MedicatieLijst {
    public ArrayList<Medicatie> list = new ArrayList<Medicatie>();

    public void add(Medicatie medic){
        list.add(medic);
    }

    public void remove(int id) {
        Medicatie medicationRemove = null;

        for (Medicatie medic : list) {
            if (medic.getId() == id) {
                medicationRemove = medic;
            }
        }
        
        list.remove(medicationRemove);
    }

}
