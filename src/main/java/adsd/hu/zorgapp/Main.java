package adsd.hu.zorgapp;

import adsd.hu.zorgapp.Controller.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;

public class Main {

    public static String basedir;
    private static final Logger logger = LogManager.getLogger(Main.class);

    public static void main(String[] args) {
        logger.info("ZorgApp started");
        File jarDir = new File(Main.class.getProtectionDomain().getCodeSource().getLocation().getPath());
        File subDir = new File(jarDir.getParent());
        basedir = subDir.getParent();

        new PatientController();
    }
}
