package adsd.hu.zorgapp.Output;

import adsd.hu.zorgapp.Main;
import adsd.hu.zorgapp.Model.*;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;

public class ReadWriteIO extends Output<PatientModel> {

    private final String path = Main.basedir;
    private final String filename = "/patienten";
    private final String ext = ".json";

    @Override
    public void Write(ArrayList<PatientModel> list) {

        objList = list;
        JSONArray allPatients = new JSONArray();

        for (PatientModel model : objList) {

            /* Personal info */
            JSONObject myPatientInfo = new JSONObject();
            myPatientInfo.put("id", model.getId());
            myPatientInfo.put("firstname", model.getFirstname());
            myPatientInfo.put("surname", model.getSurname());
            myPatientInfo.put("age", model.getAge());
            myPatientInfo.put("callName", model.getCallName());
            myPatientInfo.put("address", model.getAddress());
            myPatientInfo.put("dateOfBirth", model.getDateOfBirth().toString());
            myPatientInfo.put("weight", model.getWeight());
            myPatientInfo.put("length", model.getLength());

            /* Medication info */
            JSONArray myMedicationList = new JSONArray();
            for (Medicatie medic : model.medicatieLijst.list) {
                JSONObject myMedication = new JSONObject();

                myMedication.put("id", medic.getId());
                myMedication.put("name", medic.getName());
                myMedication.put("omschrijving", medic.getOmschrijving());
                myMedication.put("type", medic.getType());
                myMedication.put("dosering", medic.getDosering());
                myMedicationList.add(myMedication);
            }
            JSONObject myMedications = new JSONObject();
            myMedications.put("Medicatielijst", myMedicationList);

            JSONArray myWeightRegistrationList = new JSONArray();
            for (GewichtsRegistratie reg : model.gewichtsRegistratieLijst.list) {
                JSONObject registrations = new JSONObject();

                registrations.put("id", reg.getId());
                registrations.put("localdatetime", reg.geLocalDateTime().toString());
                registrations.put("weight", reg.getWeight());
                registrations.put("length", reg.getLength());
                registrations.put("bmi", reg.getBmi());
                myWeightRegistrationList.add(registrations);
            }
            JSONObject myWeightRegistrations = new JSONObject();
            myWeightRegistrations.put("WeightRegistration", myWeightRegistrationList);

            JSONArray myJsonObj = new JSONArray();
            myJsonObj.add(myPatientInfo);
            myJsonObj.add(myMedications);
            myJsonObj.add(myWeightRegistrations);
            allPatients.add(myJsonObj);
        }

        try (FileWriter file = new FileWriter(path+filename+ext, false)) {
            file.write(allPatients.toJSONString());
            file.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public ArrayList<PatientModel> Read() {
        JSONArray patientObject = null;
        JSONParser jsonParser = new JSONParser();

        try (FileReader reader = new FileReader(path+filename+ext)) {
            patientObject = (JSONArray)jsonParser.parse(reader);
        } catch (ParseException | IOException e) {
            e.printStackTrace();
        }

        ArrayList<PatientModel> patientList = new ArrayList<PatientModel>();

        for (Object obj : patientObject) {
            PatientModel patientObj = new PatientModel();
            JSONArray jArray = (JSONArray) obj;

            JSONObject jsonPartPatientInfo = (JSONObject) jArray.get(0);
            JSONObject jsonPartMedicationArray = (JSONObject) jArray.get(1);
            JSONObject jsonPartWeightRegistration = (JSONObject) jArray.get(2);

            Long id = (Long) jsonPartPatientInfo.get("id");
            patientObj.setId(id.intValue());
            patientObj.setFirstname((String) jsonPartPatientInfo.get("firstname"));
            patientObj.setSurname((String) jsonPartPatientInfo.get("surname"));
            Long ageLong = (Long) jsonPartPatientInfo.get("age");
            patientObj.setAge(ageLong.intValue());
            patientObj.setCallName((String) jsonPartPatientInfo.get("callName"));
            patientObj.setAddress((String) jsonPartPatientInfo.get("address"));
            patientObj.setDateOfBirth(LocalDate.parse((String) jsonPartPatientInfo.get("dateOfBirth")));
            patientObj.setWeight((double) jsonPartPatientInfo.get("weight"));
            patientObj.setLength((double) jsonPartPatientInfo.get("length"));

            MedicatieLijst list = new MedicatieLijst();
            JSONArray jsonMedicationArray = (JSONArray) jsonPartMedicationArray.get("Medicatielijst");
            for (Object o : jsonMedicationArray) {
                JSONObject json = (JSONObject) o;
                Medicatie medic = new Medicatie(
                        Integer.parseInt(json.get("id").toString()),
                        (String) json.get("name"),
                        (String) json.get("omschrijving"),
                        (String) json.get("type"),
                        (String) json.get("dosering"));
                list.add(medic);
            }

            GewichtsRegistratieLijst weightlist = new GewichtsRegistratieLijst();
            JSONArray jsonWeightRegArray = (JSONArray) jsonPartWeightRegistration.get("WeightRegistration");
            for (Object o : jsonWeightRegArray) {
                JSONObject json = (JSONObject) o;
                GewichtsRegistratie reg = new GewichtsRegistratie(
                        Integer.parseInt(json.get("id").toString()),
                        LocalDateTime.parse(json.get("localdatetime").toString()),
                        (double) json.get("weight"),
                        (double) json.get("length"));
                weightlist.add(reg);
            }

            patientObj.medicatieLijst = list;
            patientObj.gewichtsRegistratieLijst = weightlist;
            patientList.add(patientObj);
        }
        return patientList;
    }


}
