package adsd.hu.zorgapp.Output;

import adsd.hu.zorgapp.Model.PatientModel;

import java.util.ArrayList;

public abstract class Output<T> {

    public T obj;
    public ArrayList<T> objList;

    public abstract void Write(ArrayList<T> list);
    public abstract ArrayList<T> Read();


}
