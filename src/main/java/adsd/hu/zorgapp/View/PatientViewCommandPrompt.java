package adsd.hu.zorgapp.View;

import adsd.hu.zorgapp.Model.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.Scanner;
import static java.lang.System.out;

public class PatientViewCommandPrompt {

    final String ANSI_BLACK  ="\u001B[30m";
    final String ANSI_RED	="\u001B[31m";
    final String ANSI_YELLOW = "\u001B[33m";
    final String ANSI_GREEN	="\u001B[32m";
    final String ANSI_WHITE  ="\u001B[37m";

    private Scanner myScanner = new Scanner(System.in);
    private PatientModel model;
    private PatientenLijst patientenLijst;
    private Properties properties = new Properties();
    private Login login = new Login();
    private String output;
    private int menuItem;
    private int rol;
    private static final Logger logger = LogManager.getLogger(PatientViewCommandPrompt.class);

    public PatientViewCommandPrompt(PatientenLijst model){
        myScanner.useDelimiter(System.lineSeparator());
        this.patientenLijst = model;
        displayMenuOptions();
    }

    public void setPatientData(){
        out.print("Enter your firstname > ");
        model.setFirstname(myScanner.nextLine());

        out.print("Enter your surname > ");
        model.setSurname(myScanner.nextLine());

        try {
            out.print("Enter your date of birth(YYYY-MM-DD) > ");
            model.setDateOfBirth(LocalDate.parse(myScanner.nextLine()));
        } catch (Exception e ){
            out.println(ANSI_RED + e + " format date of birth must be YYYY-MM-DD"+ANSI_BLACK);
        }

        logger.info("Patient info modified, Firstname=" + model.getFirstname() + " Surname " + model.getSurname() + " DayOfBirth " + model.getDateOfBirth());
        if (rol==1) displayMenuOptions4HealthCareProvidersSelectedPatient();
        if (rol==2) displayMenuOptions4Patients();
    }

    public void viewAllPatients(boolean returnDirect) {
        String leftAlignFormatString = "| %-4s | %-15s | %-15s | %-15s | %-15s | %-15s | %-15s | %-15s | %-15s | %n";

        out.println(ANSI_YELLOW);
        out.format("+------+-----------------+-----------------+-----------------+-----------------+-----------------+-----------------+-----------------+-----------------+%n");
        out.format("| ID   | Firstname       | Surname         | Callname        | Address         | Date of birth   | Age             | Length          | Weight          |%n");

        for (PatientModel model : patientenLijst.list) {
            out.format("+------+-----------------+-----------------+-----------------+-----------------+-----------------+-----------------+-----------------+-----------------+%n");
            out.format(leftAlignFormatString, model.getId(), model.getFirstname(), model.getSurname(), model.getCallName(), model.getAddress(), model.getDateOfBirth(), model.getAge(), model.getLength(), model.getWeight());
        }

        out.format("+------+-----------------+-----------------+-----------------+-----------------+-----------------+-----------------+-----------------+-----------------+%n");
        out.println(ANSI_BLACK);

        logger.info("Displayed all patients");
        if (returnDirect) displayMenuOptions4HealthCareProviders();
    }

    public void viewPatientData() {
        String leftAlignFormatString = "| %-4s | %-15s | %-15s | %-15s | %-15s | %-15s | %-15s | %-15s | %-15s | %n";

        out.println(ANSI_YELLOW);
        out.format("+------+-----------------+-----------------+-----------------+-----------------+-----------------+-----------------+-----------------+-----------------+%n");
        out.format("| ID   | Firstname       | Surname         | Callname        | Address         | Date of birth   | Age             | Length          | Weight          |%n");

        out.format("+------+-----------------+-----------------+-----------------+-----------------+-----------------+-----------------+-----------------+-----------------+%n");
        out.format(leftAlignFormatString, model.getId(), model.getFirstname(), model.getSurname(), model.getCallName(), model.getAddress(), model.getDateOfBirth(), model.getAge(), model.getLength(), model.getWeight());

        out.format("+------+-----------------+-----------------+-----------------+-----------------+-----------------+-----------------+-----------------+-----------------+%n");
        out.println(ANSI_BLACK);
        logger.info("Displayed selected patient info");
    }

    public void displayMenuOptions() {
        output = "\n" + ANSI_YELLOW + "Main Menu\n";
        output += "----------------------------------\n";
        output += ANSI_GREEN + "1" + ANSI_WHITE + ". -> I am an health care provider\n";
        output += ANSI_GREEN + "2" + ANSI_WHITE + ". -> I am an patient\n";
        output += ANSI_GREEN + "3" + ANSI_WHITE + ". -> Exit program\n";
        output += "Enter choice > " + ANSI_GREEN;
        out.print(output);

        try {
            rol = Integer.parseInt(myScanner.nextLine());
        } catch (InputMismatchException | NumberFormatException e) {
            out.println(ANSI_RED + e + "InputMismatchException or NumberFormatException | you should input a number" +ANSI_BLACK);
            displayMenuOptions();
        }

        logger.info("Main menu");

        if (rol == 1 ) displayMenuOptions4HealthCareProviders();
        if (rol == 2 ) displayMenuOptions4Patients();
        if (rol == 3 ) patientenLijst.writeJsonAndExit(true);//model.writeJsonAndExit(true);
        if (rol > 3 || rol < 1) {
            out.println(ANSI_RED+"This is not an option."+ANSI_BLACK);
            displayMenuOptions();
        }

    }

    public void displayMenuOptions4HealthCareProviders() {

        boolean authorized = login.authorize(myScanner);
        if (!authorized) displayMenuOptions();

        output = "\n" + ANSI_YELLOW + "Menu - Health care providers\n";
        output += "--------------------\n";
        output += ANSI_GREEN + "1" + ANSI_WHITE + ". -> Add new patient\n";
        output += ANSI_GREEN + "2" + ANSI_WHITE + ". -> View all patients\n";
        output += ANSI_GREEN + "3" + ANSI_WHITE + ". -> Select patient\n";
        output += ANSI_GREEN + "4" + ANSI_WHITE + ". -> Go back\n";

        output += "Enter choice > " + ANSI_GREEN;
        out.print(output);

        try {
            menuItem = Integer.parseInt(myScanner.nextLine());
        } catch (InputMismatchException | NumberFormatException e) {
            out.println(ANSI_RED + e + "InputMismatchException or NumberFormatException | you should input a number" +ANSI_BLACK);
            displayMenuOptions4HealthCareProviders();
        }

        logger.info("display menu options for health care providers");

        if (menuItem == 1) addPatient();
        if (menuItem == 2) viewAllPatients(true);
        if (menuItem == 3) selectPatient();
        if (menuItem == 4) displayMenuOptions();
        if (rol > 4 || rol < 1) {
            out.println(ANSI_RED + "This is not an option." + ANSI_BLACK);
            displayMenuOptions4HealthCareProviders();
        }

    }

    private void selectPatient() {
        viewAllPatients(false);

        out.print(ANSI_WHITE + "Select patient by entering ID > ");
        int id = Integer.parseInt(myScanner.nextLine());

        for (PatientModel model : patientenLijst.list) {
            if (model.getId() == id) this.model = model;
        }

        logger.info("patient selected");
        displayMenuOptions4HealthCareProvidersSelectedPatient();
    }

    private void addPatient() {
        HashMap info = getPatientInfoFromUser();

        patientenLijst.list.add(new PatientModel(
                properties.getAndRaisePatientID(),
                info.get("firstname").toString(),
                info.get("surname").toString(),
                LocalDate.parse(info.get("dateOfBirth").toString())
        ));

        logger.info("added new patient");
        displayMenuOptions4HealthCareProviders();
    }

    private HashMap getPatientInfoFromUser(){

        HashMap<String, String> info = new HashMap<String, String>();

        out.print(ANSI_WHITE + "Enter your firstname > " + ANSI_GREEN);
        info.put("firstname", myScanner.nextLine() );

        out.print(ANSI_WHITE + "Enter your surname > " + ANSI_GREEN);
        info.put("surname", myScanner.nextLine() );

        try {
            out.print(ANSI_WHITE + "Enter your date of birth(YYYY-MM-DD) > " + ANSI_GREEN);
            info.put("dateOfBirth", myScanner.nextLine() );
        } catch (Exception e ){
            out.println(ANSI_RED + e + " format date of birth must be YYYY-MM-DD" + ANSI_WHITE);
        }

        return info;
    }

    private void addMedication(){
        out.print("Please fill in the name of the medication: ");
        String name = myScanner.nextLine();

        out.print("Please fill in the description of the medication: ");
        String omschrijving = myScanner.nextLine();

        out.print("Please fill in the type of the medication: ");
        String type = myScanner.nextLine();

        out.print("Please fill in the dosage of the medication: ");
        String dosering = myScanner.nextLine();

        model.medicatieLijst.add(new Medicatie(properties.getAndRaiseMedicationID(), name, omschrijving, type, dosering));

        logger.info("added new medication to " + model.getFirstname() + " " + model.getSurname());
        displayMenuOptions4HealthCareProvidersSelectedPatient();
    }

    private void viewMedicationList(boolean healthCareProvider){
        String leftAlignFormatString = "| %-4s | %-15s | %-15s | %-15s | %-15s | %n";

        out.println(ANSI_YELLOW);
        out.format("+------+-----------------+-----------------+-----------------+-----------------+%n");
        out.format("| ID   | Name            | Omschrijving    | Type            | Dosering        |%n");

        for (Medicatie medic : model.medicatieLijst.list) {
            out.format("+------+-----------------+-----------------+-----------------+-----------------+%n");
            out.format(leftAlignFormatString, medic.getId(), medic.getName(), medic.getOmschrijving(), medic.getType(), medic.getDosering() );
        }

        out.format("+------+-----------------+-----------------+-----------------+-----------------+%n");
        out.println(ANSI_BLACK);

        logger.info("displayed medication list");

        if (healthCareProvider) {
            displayMenuOptions4HealthCareProvidersSelectedPatient();
        } else {
            displayMenuOptions4Patients();
        }
    }

    private void deleteMedication(){
        out.print("Please fill in the id: ");
        int id = Integer.parseInt(myScanner.nextLine());
        model.medicatieLijst.remove(id);

        logger.info("deleted medication with id " + id );
        displayMenuOptions4HealthCareProvidersSelectedPatient();
    }

    public void displayMenuOptions4Patients(){
        output = "\n" + ANSI_YELLOW + "Menu - Patients\n";
        output += "--------------------\n";
        output += ANSI_GREEN + "1" + ANSI_WHITE + ". -> Set patient data\n";
        output += ANSI_GREEN + "2" + ANSI_WHITE + ". -> View patient data\n";
        output += ANSI_GREEN + "3" + ANSI_WHITE + ". -> View medications\n";
        output += ANSI_GREEN + "4" + ANSI_WHITE + ". -> Go back\n";
        output += "Enter choice > " + ANSI_GREEN;
        out.print(output);

        try {
            menuItem = Integer.parseInt(myScanner.nextLine());
        } catch (InputMismatchException | NumberFormatException e){
            out.println(ANSI_RED + e + "InputMismatchException or NumberFormatException | you should input a number" + ANSI_BLACK);
            displayMenuOptions4Patients();
        }

        logger.info("display menu options for patients");

        if (menuItem == 1) setPatientData();
        if (menuItem == 2) {
            if (model.getFirstname() != null) {
                viewPatientData();
                displayMenuOptions4Patients();
            } else {
                out.println(ANSI_RED + "First enter patient data" + ANSI_BLACK);
                displayMenuOptions4Patients();
            }
        }
        if (menuItem == 3) viewMedicationList(false);
        if (menuItem == 4) displayMenuOptions();
        if (rol > 4 || rol < 1) {
            out.println(ANSI_RED+"This is not an option."+ANSI_BLACK);
            displayMenuOptions4Patients();
        }
    }

    public void displayMenuOptions4HealthCareProvidersSelectedPatient() {

        boolean authorized = login.authorize(myScanner);
        if (!authorized) displayMenuOptions();

        output = "\n" + ANSI_YELLOW + "Menu - Health care provider: " + model.getFirstname() + " " + model.getSurname() + " \n";
        output += "--------------------\n";
        output += ANSI_GREEN + "1" + ANSI_WHITE + ". -> Set patient data\n";
        output += ANSI_GREEN + "2" + ANSI_WHITE + ". -> View patient data\n";
        output += ANSI_GREEN + "3" + ANSI_WHITE + ". -> View medications\n";
        output += ANSI_GREEN + "4" + ANSI_WHITE + ". -> Add medication\n";
        output += ANSI_GREEN + "5" + ANSI_WHITE + ". -> Delete medication\n";
        output += ANSI_GREEN + "6" + ANSI_WHITE + ". -> Register weight\n";
        output += ANSI_GREEN + "7" + ANSI_WHITE + ". -> View weight registation\n";
        output += ANSI_GREEN + "8" + ANSI_WHITE + ". -> Go back\n";
        output += "Enter choice > ";
        out.print(output);

        try {
            menuItem = Integer.parseInt(myScanner.nextLine());
        } catch (InputMismatchException | NumberFormatException e) {
            out.println(ANSI_RED + e + "InputMismatchException or NumberFormatException | you should input a number" +ANSI_BLACK);
            displayMenuOptions4HealthCareProviders();
        }

        logger.info("displayed menu options for selected patient");

        if (menuItem == 1) setPatientData();
        if (menuItem == 2) {
            if (model.getFirstname() != null) {
                viewPatientData();
                displayMenuOptions4HealthCareProvidersSelectedPatient();
            } else {
                out.println(ANSI_RED + "First enter patient data" + ANSI_BLACK);
                displayMenuOptions4HealthCareProvidersSelectedPatient();
            }
        }
        if (menuItem == 3) viewMedicationList(true);
        if (menuItem == 4) addMedication();
        if (menuItem == 5) deleteMedication();
        if (menuItem == 6) registerWeight();
        if (menuItem == 7) viewWeightRegistration();
        if (menuItem == 8) displayMenuOptions4HealthCareProviders();
    }

    int trend;
    private void viewWeightRegistration() {

        String leftAlignFormatString = "| %-20s | %-15s | %-15s | %-15s | %-100s|%n";

        out.println(ANSI_YELLOW);
        out.format("+----------------------+-----------------+-----------------+-----------------+-----------------------------------------------------------------------------------------------------+%n");
        out.format("| Date-Time            | Weigth          | Length          | Bmi             | Trend                                                                                               |%n");

        int highestBmi = 0;
        for (GewichtsRegistratie reg : model.gewichtsRegistratieLijst.list){
            if(highestBmi < reg.getBmi()) highestBmi = (int) reg.getBmi();
        }
        trend = 100 / highestBmi;

        for (GewichtsRegistratie reg : model.gewichtsRegistratieLijst.list){
            String DateTime = reg.geLocalDateTime().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
            out.format("+----------------------+-----------------+-----------------+-----------------+-----------------------------------------------------------------------------------------------------+%n");
            out.format(leftAlignFormatString, DateTime, reg.getWeight(), reg.getLength(), reg.getBmi(),  trend(reg.getBmi()));
        }

        out.format("+----------------------+-----------------+-----------------+-----------------+-----------------------------------------------------------------------------------------------------+%n");
        out.println(ANSI_BLACK);

        logger.info("displayed weight registration");
        displayMenuOptions4HealthCareProvidersSelectedPatient();
    }

    public String trend(double bmi){
        int aantal = (int)bmi * trend;
        StringBuilder stars = new StringBuilder();
        for (int i = 0; i <= aantal; i++){
            stars.append("*");
        }
        return stars.toString();
    }

    private void registerWeight() {
        out.print("Enter weight > ");
        double weight = Double.parseDouble(myScanner.nextLine());
        model.setWeight(weight);

        out.print("Enter length > ");
        double length = Double.parseDouble(myScanner.nextLine());
        model.setLength(length);

        model.gewichtsRegistratieLijst.add(new GewichtsRegistratie(properties.getAndRaiseWeightRegistrationID(), weight,length));

        logger.info("added new weight registration");
        displayMenuOptions4HealthCareProvidersSelectedPatient();
    }

    private void clearConsole(){
        System.out.print("\033[H\033[2J");
        System.out.flush();
    }

}
