import adsd.hu.zorgapp.Model.GewichtsRegistratie;
import adsd.hu.zorgapp.Model.PatientModel;
import org.junit.Test;

import java.time.LocalDate;
import static junit.framework.Assert.assertEquals;

public class PatiendModelTest {

    @Test
    public void modelFirstname() {
        PatientModel model = new PatientModel(5, "Rasim", "Sabuncu", LocalDate.parse("1994-06-16"));
        assertEquals(model.getFirstname(), "Rasim");
    }
    @Test
    public void modelSurname() {
        PatientModel model = new PatientModel(5, "Rasim", "Sabuncu", LocalDate.parse("1994-06-16"));
        assertEquals(model.getSurname(), "Sabuncu");
    }
    @Test
    public void modelAge() {
        PatientModel model = new PatientModel(5, "Rasim", "Sabuncu", LocalDate.parse("1994-06-16"));
        assertEquals(model.getAge(), 27);
    }
    @Test
    public void calcBmi() {
        GewichtsRegistratie reg = new GewichtsRegistratie(5, 81,171.00);
        assertEquals(reg.getBmi(), 27.7);
    }
}
